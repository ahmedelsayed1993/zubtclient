package com.aait.zubtclient.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ProvidersModel implements Serializable {
    private ArrayList<ProviderModel> providers;
    private PaginationModel pagination;

    public ArrayList<ProviderModel> getProviders() {
        return providers;
    }

    public void setProviders(ArrayList<ProviderModel> providers) {
        this.providers = providers;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
