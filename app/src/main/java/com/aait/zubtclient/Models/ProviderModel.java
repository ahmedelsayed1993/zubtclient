package com.aait.zubtclient.Models;

import java.io.Serializable;

public class ProviderModel implements Serializable {
    private int id;
    private String name;
    private String avatar;
    private String lat;
    private String lng;
    private String city;
    private String rate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String  getRate() {
        return rate;
    }

    public void setRate(String  rate) {
        this.rate = rate;
    }
}
