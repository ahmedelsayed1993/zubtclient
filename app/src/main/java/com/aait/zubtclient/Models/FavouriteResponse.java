package com.aait.zubtclient.Models;

public class FavouriteResponse extends BaseResponse {
    private FavouriteModel data;

    public FavouriteModel getData() {
        return data;
    }

    public void setData(FavouriteModel data) {
        this.data = data;
    }
}
