package com.aait.zubtclient.Models;

public class HomeResponse extends BaseResponse{
    private HomeModel data;

    public HomeModel getData() {
        return data;
    }

    public void setData(HomeModel data) {
        this.data = data;
    }
}
