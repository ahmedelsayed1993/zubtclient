package com.aait.zubtclient.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class OrdersModel implements Serializable {
    private ArrayList<OrderModel> orders;
    private PaginationModel pagination;

    public ArrayList<OrderModel> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<OrderModel> orders) {
        this.orders = orders;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
