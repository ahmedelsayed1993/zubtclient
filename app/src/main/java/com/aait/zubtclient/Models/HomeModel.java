package com.aait.zubtclient.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class HomeModel implements Serializable {
    private ArrayList<CategoryModel> categories;
    private ArrayList<String> banners;
    private int notifications;

    public ArrayList<CategoryModel> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<CategoryModel> categories) {
        this.categories = categories;
    }

    public ArrayList<String> getBanners() {
        return banners;
    }

    public void setBanners(ArrayList<String> banners) {
        this.banners = banners;
    }

    public int getNotifications() {
        return notifications;
    }

    public void setNotifications(int notifications) {
        this.notifications = notifications;
    }
}
