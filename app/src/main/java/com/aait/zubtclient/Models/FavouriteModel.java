package com.aait.zubtclient.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class FavouriteModel implements Serializable {
    private ArrayList<ProviderModel> favourites;
    private PaginationModel pagination;

    public ArrayList<ProviderModel> getFavourites() {
        return favourites;
    }

    public void setFavourites(ArrayList<ProviderModel> favourites) {
        this.favourites = favourites;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
