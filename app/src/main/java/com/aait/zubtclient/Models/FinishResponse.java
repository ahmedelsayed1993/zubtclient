package com.aait.zubtclient.Models;

public class FinishResponse extends BaseResponse {
    private FinishModel data;

    public FinishModel getData() {
        return data;
    }

    public void setData(FinishModel data) {
        this.data = data;
    }
}
