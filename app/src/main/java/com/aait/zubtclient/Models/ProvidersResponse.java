package com.aait.zubtclient.Models;

public class ProvidersResponse extends BaseResponse {
    private ProvidersModel data;

    public ProvidersModel getData() {
        return data;
    }

    public void setData(ProvidersModel data) {
        this.data = data;
    }
}
