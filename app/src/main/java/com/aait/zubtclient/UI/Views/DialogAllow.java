package com.aait.zubtclient.UI.Views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.aait.zubtclient.App.Constant;
import com.aait.zubtclient.Pereferences.LanguagePrefManager;
import com.aait.zubtclient.Pereferences.SharedPrefManager;
import com.aait.zubtclient.R;
import com.aait.zubtclient.Uitls.PermissionUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.aait.zubtclient.App.Constant.RequestPermission.REQUEST_IMAGES;

public class DialogAllow extends Activity {
    Button allow;

        @Override

        protected void onCreate(Bundle savedInstanceState) {


            super.onCreate(savedInstanceState);

            requestWindowFeature(Window.FEATURE_NO_TITLE);

            setContentView(R.layout.dialog_allow);
            allow = findViewById(R.id.allow);
            allow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, Constant.RequestCode.GPS_ENABLING);
                }
            });

        }


}
