package com.aait.zubtclient.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.zubtclient.Base.BaseFragment;
import com.aait.zubtclient.Listeners.OnItemClickListener;
import com.aait.zubtclient.Listeners.PaginationScrollListener;
import com.aait.zubtclient.Models.ProviderModel;
import com.aait.zubtclient.Models.ProvidersResponse;
import com.aait.zubtclient.Models.QuestionModel;
import com.aait.zubtclient.Models.QuestionResponse;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Activities.ProviderDetailsActivity;
import com.aait.zubtclient.UI.Activities.ProvidersActivity;
import com.aait.zubtclient.UI.Adapters.ProviderAdapter;
import com.aait.zubtclient.UI.Adapters.QuestionsAdapter;
import com.aait.zubtclient.UI.Views.BottomNavigationViewHelper;
import com.aait.zubtclient.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProvidersFragment extends BaseFragment implements OnItemClickListener {
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;

    LinearLayoutManager linearLayoutManager;
    ArrayList<ProviderModel> providerModels = new ArrayList<>();
    ProviderAdapter providerAdapter;
    ArrayList<ProviderModel> id = new ArrayList<>();
    String cat;
    String lat;
    String lng;
    String filter;
    String name;


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_providers;
    }
    public static ProvidersFragment newInstance(String lat,String lng,String filter,String name,String cat) {
        Bundle args = new Bundle();
        ProvidersFragment fragment = new ProvidersFragment();
        args.putString("cat",cat);
        args.putString("lat",lat);
        args.putString("lng",lng);
        args.putString("filter",filter);
        args.putString("name",name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initializeComponents(View view) {
        Bundle bundle = this.getArguments();
        cat = bundle.getString("cat");
        lat = bundle.getString("lat");
        lng = bundle.getString("lng");
        filter = bundle.getString("filter");
        name = bundle.getString("name");
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        providerAdapter = new ProviderAdapter(mContext,providerModels,R.layout.recycler_provider);
//        providerAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(providerAdapter);
        getQuestions(name,cat,filter,lat,lng);

//             if (id.size()==0){
//                 layNoItem.setVisibility(View.VISIBLE);
//                 tvNoContent.setText(getString(R.string.content_not_found_you_can_still_search_the_app_freely));
//             }else {
//                 layNoItem.setVisibility(View.GONE);
//                 providerAdapter.updateAll(id);
//             }





    }


    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {


    }
    private void getQuestions(String word,String cat,String filter,String lat,String lng){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProviders(mLanguagePrefManager.getAppLanguage(),word,cat,filter,lat,lng).enqueue(new Callback<ProvidersResponse>() {
            @Override
            public void onResponse(Call<ProvidersResponse> call, Response<ProvidersResponse> response) {
                layProgress.setVisibility(View.GONE);
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getValue().equals("1")) {
                        if (response.body().getData().getProviders().size()!=0) {
                            providerAdapter.updateAll(response.body().getData().getProviders());
                        }else {
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProvidersResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);

            }
        });
    }

}
