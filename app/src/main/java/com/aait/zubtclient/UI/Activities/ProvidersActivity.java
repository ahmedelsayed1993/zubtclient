package com.aait.zubtclient.UI.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aait.zubtclient.App.Constant;
import com.aait.zubtclient.Base.ParentActivity;
import com.aait.zubtclient.Gps.GPSTracker;
import com.aait.zubtclient.Gps.GpsTrakerListener;
import com.aait.zubtclient.Listeners.OnItemClickListener;
import com.aait.zubtclient.Models.CategoryModel;
import com.aait.zubtclient.Models.CityModel;
import com.aait.zubtclient.Models.ProviderModel;
import com.aait.zubtclient.Models.ProvidersResponse;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Adapters.ProviderTapAdapter;
import com.aait.zubtclient.UI.Adapters.SubscribeTapAdapter;
import com.aait.zubtclient.UI.Fragments.MapFragment;
import com.aait.zubtclient.UI.Fragments.ProvidersFragment;
import com.aait.zubtclient.UI.Views.BottomNavigationViewHelper;
import com.aait.zubtclient.UI.Views.DialogAllow;
import com.aait.zubtclient.UI.Views.ListDialog;
import com.aait.zubtclient.Uitls.CommonUtil;
import com.aait.zubtclient.Uitls.DialogUtil;
import com.aait.zubtclient.Uitls.PermissionUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Stack;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProvidersActivity extends ParentActivity implements OnItemClickListener, BottomNavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemReselectedListener, GpsTrakerListener {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;
    GoogleMap googleMap;

    Marker myMarker;

    GPSTracker gps;

    public String mLang, mLat;

    private AlertDialog mAlertDialog;

    boolean startTracker = false;

@BindView(R.id.home_fragment_container)
FrameLayout home_fragment_container;
    @OnClick(R.id.notification)
    void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    private ProviderTapAdapter mAdapter;
    CategoryModel categoryModel;
    CityModel cityModel;
    ArrayList<CityModel> cityModels=new ArrayList<>();
    ListDialog listDialog;
    @BindView(R.id.search)
    EditText search;
    ProvidersFragment providersFragment;
    MapFragment mapFragment;
    private Stack<Integer> tabsStack = new Stack<>();

    private FragmentManager fragmentManager;

    private FragmentTransaction transaction;
    int selectedTab = 0;
    Geocoder geocoder;
    @Override
    protected void initializeComponents() {


        cityModels.add(0,new CityModel("near",getString(R.string.nearest)));
        cityModels.add(1,new CityModel("high_rate",getString(R.string.top_rate)));
        cityModels.add(2,new CityModel("low_rate",getString(R.string.low_rate)));
        categoryModel = (CategoryModel) getIntent().getSerializableExtra("cat");
        //BottomNavigationViewHelper.disableShiftMode(bottomNavigation);
        act_title.setText(categoryModel.getValue());
        getQuestions("",categoryModel.getId()+"",null,null,null);




    }
    @OnClick(R.id.filter)
    void onFilter(){
       listDialog =  new ListDialog(mContext,ProvidersActivity.this,cityModels,getString(R.string.sort_by));
       listDialog.show();
    }

    @OnClick(R.id.searc)
    void onSearch(){
        if (search.getText().toString().equals("")){

        }else {
            showHome(false);
            showMenu(false);
            getQuestions(search.getText().toString(),categoryModel.getId()+"",null,null,null);
        }
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_providers;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        listDialog.dismiss();
        if (view.getId()==R.id.tv_row_title) {
            if (position == 1 || position == 2) {
                Log.e("position", cityModels.get(position).getKey());
                showHome(false);
                showMenu(false);
                getQuestions(null, categoryModel.getId() + "", cityModels.get(position).getKey(), null, null);
            } else if (position == 0) {
                showHome(false);
                showMenu(false);
               getLocationWithPermission();
            }
        }

    }
    private void getQuestions(String word,String cat,String filter,String lat,String lng){
//        showProgressDialog(getString(R.string.please_wait));
//        home_fragment_container.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
//        RetroWeb.getClient().create(ServiceApi.class).getProviders(mLanguagePrefManager.getAppLanguage(),word,cat,filter,lat,lng).enqueue(new Callback<ProvidersResponse>() {
//            @Override
//            public void onResponse(Call<ProvidersResponse> call, Response<ProvidersResponse> response) {
//                hideProgressDialog();
//                if (response.isSuccessful()) {
//                    if (response.body().getValue().equals("1")) {

                        providersFragment = ProvidersFragment.newInstance(lat,lng,filter,word,cat);
                        mapFragment = MapFragment.newInstance(word,cat,filter,lat,lng);

                        fragmentManager = getSupportFragmentManager();
                        transaction = fragmentManager.beginTransaction();
                        transaction.add(R.id.home_fragment_container, providersFragment);
                       // transaction.add(R.id.home_fragment_container,mapFragment);
                        transaction.commit();
                        bottomNavigation.setOnNavigationItemSelectedListener(ProvidersActivity.this);
                        bottomNavigation.setOnNavigationItemReselectedListener(ProvidersActivity.this);
                        bottomNavigation.setSelectedItemId(R.id.list);
                        showHome(true);
                       // BottomNavigationViewHelper.disableShiftMode(bottomNavigation);


//                    }else {
//                        CommonUtil.makeToast(mContext,response.body().getMsg());
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ProvidersResponse> call, Throwable t) {
//                CommonUtil.handleException(mContext, t);
//                t.printStackTrace();
//                hideProgressDialog();
////                layNoInternet.setVisibility(View.VISIBLE);
////                layNoItem.setVisibility(View.GONE);
////                layProgress.setVisibility(View.GONE);
////                swipeRefresh.setRefreshing(false);
//            }
//        });
    }
    private void showHome(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }

        transaction = fragmentManager.beginTransaction();
       // transaction.hide(mapFragment);
        transaction.replace(R.id.home_fragment_container,providersFragment);


        transaction.commit();
        selectedTab = R.id.list;



    }
    private void showMenu(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }

        transaction = fragmentManager.beginTransaction();
     //  transaction.hide(providersFragment);
        transaction.replace(R.id.home_fragment_container,mapFragment);
        transaction.commit();
        selectedTab = R.id.map;



    }
    @Override
    public void onNavigationItemReselected(@NonNull MenuItem menuItem) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.list:
                showHome(true);
                break;
            case R.id.map:
                showMenu(true);
                break;

        }
        return true;
    }

    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        Log.e("Direction", "Direction Success");
        // dismiss traker dialog
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                Log.e("LATLNG", "Lat:" + mLat + "  Lng:" + Double.toString(log));
                //putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {

    }
    public void getLocationWithPermission() {
        gps = new GPSTracker(mContext, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(getApplicationContext(), PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }
    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
//            mAlertDialog = DialogUtil.showAlertDialog(getApplicationContext(),
//                    getString(R.string.gps_detecting), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(final DialogInterface dialogInterface, final int i) {
//                            mAlertDialog.dismiss();
                           CommonUtil.gpsDialog(this);

        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude());
                mLat = String.valueOf(gps.getLatitude());
                mLang = String.valueOf(gps.getLongitude());
                List<Address> addresses;
                geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    if (addresses.isEmpty()) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    }
//                    else{mResult = addresses.get(0).getAddressLine(0);
//                        // txt_location.setText(mResult);
//                    }

                } catch (IOException e) {
                }
                getQuestions(null,categoryModel.getId()+"","near",mLat,mLang);
                //  googleMap.clear();
                // putMapMarker(gps.getLatitude(), gps.getLongitude());
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
        } else { showGPSDisabledAlertToUser(); }
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.gps_disabled))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.enable_gps),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
//        alertDialogBuilder.setNegativeButton("Cancel",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                    }
//                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


}
