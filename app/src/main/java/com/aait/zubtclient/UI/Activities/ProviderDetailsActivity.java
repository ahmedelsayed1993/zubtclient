package com.aait.zubtclient.UI.Activities;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aait.zubtclient.Base.ParentActivity;
import com.aait.zubtclient.Base.ParentRecyclerViewHolder;
import com.aait.zubtclient.Listeners.OnItemClickListener;
import com.aait.zubtclient.Models.BaseResponse;
import com.aait.zubtclient.Models.ProviderDetialsResponse;
import com.aait.zubtclient.Models.ReviewsModels;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Adapters.ReviewsAdapter;
import com.aait.zubtclient.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProviderDetailsActivity extends ParentActivity implements OnItemClickListener {
    String id;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.rate)
    TextView rate;
    @BindView(R.id.rating)
    RatingBar rating;
    @BindView(R.id.city)
    TextView city;
    @BindView(R.id.work_times)
    TextView work_times;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.block)
    ImageView block;
    @BindView(R.id.like)
    ImageView like;
    @BindView(R.id.rv_recycle)
    RecyclerView rv_recycle;
    @OnClick(R.id.act_back)
            void onBack(){
        onBackPressed();
    }
    @OnClick(R.id.notification)
            void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }
    @BindView(R.id.act_title)
            TextView act_title;
    LinearLayoutManager linearLayoutManager;
    ArrayList<ReviewsModels> reviewsModels = new ArrayList<>();
    ReviewsAdapter reviewsAdapter;
    @BindView(R.id.filter)
    ImageView filter;
    @Override
    protected void initializeComponents() {
        filter.setVisibility(View.GONE);
        id = getIntent().getStringExtra("id");
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        reviewsAdapter = new ReviewsAdapter(mContext,reviewsModels,R.layout.recycler_reviews);
        reviewsAdapter.setOnItemClickListener(this);
        rv_recycle.setLayoutManager(linearLayoutManager);
        rv_recycle.setAdapter(reviewsAdapter);
        if (mSharedPrefManager.getLoginStatus()){
            getProvider(mSharedPrefManager.getUserData().getApi_token());
        }else {
            getProvider(null);
        }

    }
    @OnClick(R.id.like)
    void onLike(){
        if (mSharedPrefManager.getLoginStatus()){
            AddFav(mSharedPrefManager.getUserData().getApi_token());
        }else {
CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }
    @OnClick(R.id.request)
    void onRequest(){
        if (mSharedPrefManager.getLoginStatus()) {
            Intent intent = new Intent(mContext, SendRequestActivity.class);
            intent.putExtra("id", id);
            startActivity(intent);
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_provider_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getProvider(String token){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProvider(mLanguagePrefManager.getAppLanguage(),token,id).enqueue(new Callback<ProviderDetialsResponse>() {
            @Override
            public void onResponse(Call<ProviderDetialsResponse> call, Response<ProviderDetialsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        Glide.with(mContext).load(response.body().getData().getAvatar()).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter()).into(image);
                        name.setText(response.body().getData().getName());
                        act_title.setText(response.body().getData().getName());
                        rate.setText(response.body().getData().getRate()+"");
                        rating.setRating(response.body().getData().getRate());
                        city.setText(response.body().getData().getLocation());
                        if (mLanguagePrefManager.getAppLanguage().equals("ar")){
                            work_times.setText(response.body().getData().getWork_to()+" : "+response.body().getData().getWork_from());

                        }else {
                            work_times.setText(response.body().getData().getWork_from() + " : " + response.body().getData().getWork_to());

                        }
                        description.setText(response.body().getData().getDetails());
                        if (response.body().getData().getIs_favourite()==1){
                            like.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.likee));
                        }else {
                            like.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.like));
                        }
                        if (response.body().getData().getIs_reported()==1){
                            block.setVisibility(View.GONE);
                        }else {
                            block.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.flag));
                        }
                        if (response.body().getData().getReviews().isEmpty()){
                            rv_recycle.setVisibility(View.GONE);
                        }else {
                            rv_recycle.setVisibility(View.VISIBLE);
                            reviewsAdapter.updateAll(response.body().getData().getReviews());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderDetialsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
    @OnClick(R.id.block)
    void onBlock(){
        if (mSharedPrefManager.getLoginStatus()) {
            showProgressDialog(getString(R.string.please_wait));
            RetroWeb.getClient().create(ServiceApi.class).reportProvider(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getApi_token(), id).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    hideProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().getValue().equals("1")) {
                            block.setVisibility(View.GONE);

                        } else {
                            CommonUtil.makeToast(mContext, response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext, t);
                    t.printStackTrace();
                    hideProgressDialog();

                }
            });
        }
        else {

        }
    }
    private void AddFav(String token){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).addFav(mLanguagePrefManager.getAppLanguage(),token,id).enqueue(new Callback<ProviderDetialsResponse>() {
            @Override
            public void onResponse(Call<ProviderDetialsResponse> call, Response<ProviderDetialsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        if (response.body().getData().getIs_favourite()==1){
                            like.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.likee));
                        }else {
                            like.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.like));
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderDetialsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onItemClick(final View view, int position) {
        if (view.getId() == R.id.block){
            if (reviewsModels.get(position).getIs_reported()==1){

            }else {
                if (mSharedPrefManager.getLoginStatus()) {
                    showProgressDialog(getString(R.string.please_wait));
                    RetroWeb.getClient().create(ServiceApi.class).repotreview(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getApi_token(), reviewsModels.get(position).getId()).enqueue(
                            new Callback<BaseResponse>() {
                                @Override
                                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                    hideProgressDialog();
                                    if (response.isSuccessful()) {
                                        if (response.body().getValue().equals("1")) {
                                            getProvider(mSharedPrefManager.getUserData().getApi_token());
                                        } else {
                                            CommonUtil.makeToast(mContext, response.body().getMsg());
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<BaseResponse> call, Throwable t) {
                                    CommonUtil.handleException(mContext, t);
                                    t.printStackTrace();
                                    hideProgressDialog();

                                }
                            }
                    );
                }else {
                    CommonUtil.makeToast(mContext,getString(R.string.must_login));
                }
            }
        }
    }
}
