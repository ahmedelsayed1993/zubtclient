package com.aait.zubtclient.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aait.zubtclient.Models.BaseResponse;
import com.aait.zubtclient.Models.FinishModel;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.Pereferences.LanguagePrefManager;
import com.aait.zubtclient.Pereferences.SharedPrefManager;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Activities.MainActivity;
import com.aait.zubtclient.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotifyDailog extends Dialog {
    String  finishModel;
    Context mContext;
    SharedPrefManager mSharedPrefManager;
    LanguagePrefManager mLanguagePrefManager;
    @BindView(R.id.text)
    TextView text;

    public NotifyDailog(@NonNull Context context, String  text) {
        super(context);
        this.mContext=context;
        this.finishModel = text;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dailog_notify);
        getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        mSharedPrefManager = new SharedPrefManager(mContext);
        mLanguagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {
        text.setText(finishModel);

    }

}

