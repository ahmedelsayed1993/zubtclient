package com.aait.zubtclient.UI.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.zubtclient.Base.ParentActivity;
import com.aait.zubtclient.Listeners.PaginationScrollListener;
import com.aait.zubtclient.Models.FavouriteResponse;
import com.aait.zubtclient.Models.ProviderModel;
import com.aait.zubtclient.Models.ProvidersResponse;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Adapters.FavouriteAdapter;
import com.aait.zubtclient.UI.Adapters.ProviderAdapter;
import com.aait.zubtclient.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteActivity extends ParentActivity {
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @OnClick(R.id.notification)
    void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.search)
    EditText search;

    LinearLayoutManager linearLayoutManager;
    ArrayList<ProviderModel> providerModels = new ArrayList<>();
    FavouriteAdapter providerAdapter;
    // Indicates if footer ProgressBar is shown (i.e. next page is loading)
    private boolean isLoading = false;

    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES;

    // indicates the current page which Pagination is fetching.
    private int currentPage = 1;
    @BindView(R.id.filter)
    ImageView filter;
    @Override
    protected void initializeComponents() {
        filter.setVisibility(View.GONE);
        act_title.setText(getString(R.string.favourite));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        providerAdapter = new FavouriteAdapter(mContext,providerModels,R.layout.recycler_favourite);
//        providerAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(providerAdapter);


        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                search.setText("");
                currentPage = 1;
                providerModels.clear();
                isLastPage = false;
                isLoading = false;
                getQuestions(null);
            }
        });
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                getQuestions(null);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }
    @Override
    public void onResume() {
        super.onResume();
        currentPage = 1;
        providerModels.clear();
        isLastPage = false;
        isLoading = false;
        getQuestions(null);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_favourite;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.searc)
    void onSearch(){
        if (search.getText().toString().equals("")){

        }else {

                    getQuestions(search.getText().toString());

        }
    }
    private void getQuestions(String word){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getFavourite(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token(),currentPage,word).enqueue(new Callback<FavouriteResponse>() {
            @Override
            public void onResponse(Call<FavouriteResponse> call, Response<FavouriteResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){

                        TOTAL_PAGES = response.body().getData().getPagination().getTotal_pages();
                        if (currentPage == 1 &&response.body().getData().getFavourites().isEmpty()){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_fav);
                        }
                        if (currentPage < TOTAL_PAGES) {
                            if (response.body().getData().getFavourites().isEmpty()) {
                                providerAdapter.updateAll( response.body().getData().getFavourites());
                            } else {
                                providerAdapter.InsertAll( response.body().getData().getFavourites());
                            }
                            isLoading = false;
                        }else if (currentPage == TOTAL_PAGES){
                            if (response.body().getData().getFavourites().isEmpty()) {
                                providerAdapter.updateAll( response.body().getData().getFavourites());
                                isLastPage = true;
                            } else {
                                providerAdapter.InsertAll( response.body().getData().getFavourites());
                            }
                        }
                        else {
                            isLastPage = true;
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<FavouriteResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }
}
