package com.aait.zubtclient.UI.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Fragments.FinishedOrdersFragment;
import com.aait.zubtclient.UI.Fragments.OrderUnderProcessFragment;
import com.aait.zubtclient.UI.Fragments.OrdersUnderConfirmFragment;


public class SubscribeTapAdapter extends FragmentPagerAdapter {

    private Context context;

    public SubscribeTapAdapter(Context context , FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return new OrdersUnderConfirmFragment();
        }else if (position == 1){
            return new OrderUnderProcessFragment();
        }else {
            return new FinishedOrdersFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0){
            return context.getString(R.string.orders_under_confirm);
        }else if (position == 1){
            return context.getString(R.string.orders_under_process);
        }else {
            return context.getString(R.string.finished_orders);
        }
    }
}
