package com.aait.zubtclient.UI.Activities;

/**
 * created by ahmed el_sayed  7/10/2019
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.widget.EditText;

import com.aait.zubtclient.Base.ParentActivity;
import com.aait.zubtclient.Models.LoginResponse;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends ParentActivity {
    String newToken= null;
    @OnClick(R.id.forgot_pass)
    void onForgot(){
        startActivity(new Intent(mContext,ForgotPasswordActivity.class));
    }
    @OnClick(R.id.register)
    void onRegister(){
        startActivity(new Intent(mContext,RegisterActivity.class));
    }
    @OnClick(R.id.login)
    void onLogin(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.enter_phone))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,password,getString(R.string.enter_password))){
            return;
        }else {
            Login();
        }
    }
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.password)
    EditText password;
    @OnClick(R.id.view)
    void onView(){

            password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

    }
    @Override
    protected void initializeComponents() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( LoginActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });
    }
    @OnClick(R.id.visitor)
    void onVistor(){
        mSharedPrefManager.setLoginStatus(false);
        startActivity(new Intent(mContext,MainActivity.class));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void Login(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).Login(mLanguagePrefManager.getAppLanguage(),phone.getText().toString(),password.getText().toString(),newToken,"android","client").
                enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()){
                            if (response.body().getValue().equals("1")){
                                if (response.body().getStatus().equals("non-active")){
                                    CommonUtil.makeToast(mContext,response.body().getMsg());
                                    Intent intent = new Intent(mContext,ActivateAccountActivity.class);
                                    intent.putExtra("token",response.body().getData().getApi_token());
                                    intent.putExtra("code",response.body().getData().getCode());
                                    startActivity(intent);
                                }else {
                                    mSharedPrefManager.setLoginStatus(true);
                                    mSharedPrefManager.setUserData(response.body().getData());
                                    startActivity(new Intent(mContext,MainActivity.class));

                                }
                            }else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext,t);
                        t.printStackTrace();
                        hideProgressDialog();

                    }
                });

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
        } else { showGPSDisabledAlertToUser(); }
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.gps_disabled))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.enable_gps),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
//        alertDialogBuilder.setNegativeButton("Cancel",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                    }
//                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
}
