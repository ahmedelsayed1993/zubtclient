package com.aait.zubtclient.UI.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.aait.zubtclient.Models.ProviderModel;
import com.aait.zubtclient.UI.Fragments.MapFragment;

import com.aait.zubtclient.UI.Fragments.ProvidersFragment;

import java.util.ArrayList;

public class ProviderTapAdapter extends FragmentPagerAdapter {

    private Context context;
    private ArrayList<ProviderModel> id;

    public ProviderTapAdapter(Context context , FragmentManager fm ,ArrayList<ProviderModel> id) {
        super(fm);
        this.context = context;
        this.id = id;
    }

    @Override
    public Fragment getItem(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0){
            return "";
        }else {
            return "";
        }
    }
}

