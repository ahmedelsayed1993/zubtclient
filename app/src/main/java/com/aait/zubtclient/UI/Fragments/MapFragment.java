package com.aait.zubtclient.UI.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.aait.zubtclient.App.Constant;
import com.aait.zubtclient.Base.BaseFragment;
import com.aait.zubtclient.Gps.GPSTracker;
import com.aait.zubtclient.Gps.GpsTrakerListener;
import com.aait.zubtclient.Models.ProviderModel;
import com.aait.zubtclient.Models.ProvidersResponse;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Activities.ProviderDetailsActivity;
import com.aait.zubtclient.Uitls.CommonUtil;
import com.aait.zubtclient.Uitls.DialogUtil;
import com.aait.zubtclient.Uitls.PermissionUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapFragment extends BaseFragment implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback, GpsTrakerListener {

    @BindView(R.id.map)
    MapView mapView;

    GoogleMap googleMap;
    Marker myMarker , shopMarker;
    Geocoder geocoder;
    GPSTracker gps;
    HashMap<Marker, ProviderModel> hmap = new HashMap<>();
    public String mLang= "", mLat="";
    MarkerOptions markerOptions,markerOptions1;
    private AlertDialog mAlertDialog;
    ArrayList<ProviderModel> shopModels = new ArrayList<>();
    boolean startTracker = false;
    ArrayList<ProviderModel> id = new ArrayList<>();
    @BindView(R.id.lay)
    LinearLayout lay;
    String cat;
    String lat;
    String lng;
    String filter;
    String name;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_map;
    }
    public static MapFragment newInstance(String word,String cat,String filter,String lat,String lng) {
        Bundle args = new Bundle();
        MapFragment fragment = new MapFragment();
        args.putString("word",word);
        args.putString("cat",cat);
        args.putString("filter",filter);
        args.putString("lat",lat);
        args.putString("lng",lng);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initializeComponents(View view) {
        lay.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        Bundle bundle = this.getArguments();
        cat = bundle.getString("cat");
        lat = bundle.getString("lat");
        lng = bundle.getString("lng");
        filter = bundle.getString("filter");
        name = bundle.getString("word");
        mapView.onCreate(mSavedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {
        startTracker = true;
        showProgressDialog(getString(R.string.please_wait));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (!marker.getTitle().equals("موقعي")){
            if (hmap.containsKey(marker)){
                final ProviderModel myShopModel =hmap.get(marker);


                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent intent = new Intent(mContext, ProviderDetailsActivity.class);
                        intent.putExtra("lat",lat);
                        intent.putExtra("lng",lng);
                        Log.e("id", String.valueOf(myShopModel.getId()));
                        intent.putExtra("id",myShopModel.getId()+"");
                        startActivity(intent);
                    }
                });

            }else {
                googleMap.setInfoWindowAdapter(null);
            }
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
//        googleMap.setOnMapClickListener(this);
        googleMap.setOnMarkerClickListener(this);
//        getCurrentLocation();
        getLocationWithPermission();
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
                // Do something after 5s = 5000ms
                getQuestions(name,cat,filter,lat,lng);
//            }
//        }, 500);

    }
    public void putMapMarker(Double lat, Double log) {
        LatLng latLng = new LatLng(lat, log);
        myMarker = googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.maps_and_flags)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f));
// kkjgj

    }

private void getQuestions(String word,String cat,String filter,String lat,String lng){
  //showProgressDialog(getString(R.string.please_wait));

    RetroWeb.getClient().create(ServiceApi.class).getProviders(mLanguagePrefManager.getAppLanguage(),word,cat,filter,lat,lng).enqueue(new Callback<ProvidersResponse>() {
        @Override
        public void onResponse(Call<ProvidersResponse> call, Response<ProvidersResponse> response) {
           // hideProgressDialog();

            if (response.isSuccessful()) {
                if (response.body().getValue().equals("1")) {
                    if (response.body().getData().getProviders().isEmpty()) {
                            } else {
                                for ( int i = 0;i<response.body().getData().getProviders().size();i++) {
                                    addShop(response.body().getData().getProviders().get(i));
                                }
                            }

                }else {
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                }
            }
        }

        @Override
        public void onFailure(Call<ProvidersResponse> call, Throwable t) {
            CommonUtil.handleException(mContext, t);
            t.printStackTrace();
           // hideProgressDialog();


        }
    });
}
    void addShop(ProviderModel shopModel ){
        putMapMarker(Double.parseDouble(mLat),Double.parseDouble(mLang));
        markerOptions = new MarkerOptions().position(new LatLng(Double.parseDouble(shopModel.getLat()), Double.parseDouble(shopModel.getLng())))
                .title(shopModel.getName())
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.group_5326));
        shopMarker = googleMap.addMarker(markerOptions);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(shopModel.getLat()), Double.parseDouble(shopModel.getLng())),10f));
        hmap.put(shopMarker,shopModel);

    }

    public void getLocationWithPermission() {
        gps = new GPSTracker(mContext, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(getActivity(), PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }

    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
//            mAlertDialog = DialogUtil.showAlertDialog(getActivity(),
//                    getString(R.string.gps_detecting), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(final DialogInterface dialogInterface, final int i) {
//                            mAlertDialog.dismiss();
//                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                            startActivityForResult(intent, Constant.RequestCode.GPS_ENABLING);
//                        }
//                    });
            CommonUtil.gpsDialog(getActivity());
        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude());
                mLat = String.valueOf(gps.getLatitude());
                mLang = String.valueOf(gps.getLongitude());
//                List<Address> addresses;
//                geocoder = new Geocoder(getActivity(), Locale.getDefault());
//                try {
//                    addresses = geocoder.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
//                    if (addresses.isEmpty()){
//                        Toast.makeText(getActivity(), getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
//                    }
////                    else{mResult = addresses.get(0).getAddressLine(0);
////                        // txt_location.setText(mResult);
////                    }
//
//                } catch (IOException e) {}
                googleMap.clear();
                putMapMarker(gps.getLatitude(), gps.getLongitude());

            }
        }
    }
}
