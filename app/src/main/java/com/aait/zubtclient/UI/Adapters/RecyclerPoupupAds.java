package com.aait.zubtclient.UI.Adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.aait.zubtclient.Base.ParentRecyclerAdapter;
import com.aait.zubtclient.Base.ParentRecyclerViewHolder;
import com.aait.zubtclient.Models.CityModel;
import com.aait.zubtclient.R;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class RecyclerPoupupAds extends PagerAdapter {

    Context context;
    List<String> data;
    // private ImageView.ScaleType scaleType;
    private LayoutInflater layoutInflater;

    public RecyclerPoupupAds(Context context, List<String>data) {
        this.context = context;

        this.data=data;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View imageLayout = layoutInflater.inflate(R.layout.recycler_image,null);
        // assert imageLayout != null;
        final ImageView imageView = imageLayout.findViewById(R.id.food_img);

        Glide.with(context).load(data.get(position)).into(imageView);
        ViewPager vp =(ViewPager) container;
        vp.addView(imageLayout,0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public static class ListDialogAdapter extends ParentRecyclerAdapter<CityModel> {

        public ListDialogAdapter(final Context context, final List<CityModel> data) {
            super(context, data);
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        @Override
        public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            ParentRecyclerViewHolder viewHolder = null;
            View viewItem = inflater.inflate(R.layout.recycler_dialog_list_row, parent, false);
            viewHolder = new ListAdapter(viewItem);

            return viewHolder;
        }


        @Override
        public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
            ListAdapter listAdapter = (ListAdapter) holder;
            CityModel listModel = data.get(position);
            listAdapter.tv_row_title.setText(listModel.getValue());
            if (position % 2 == 0) {
                listAdapter.tv_row_title.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorWhite));
            } else {
                listAdapter.tv_row_title.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorOffWhite));

            }

            listAdapter.tv_row_title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    itemClickListener.onItemClick(view, position);
                }
            });
        }


        protected class ListAdapter extends ParentRecyclerViewHolder {

            @BindView(R.id.tv_row_title)
            TextView tv_row_title;

            public ListAdapter(View itemView) {
                super(itemView);
                setClickableRootView(itemView);
            }
        }
    }
}
