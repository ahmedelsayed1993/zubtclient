package com.aait.zubtclient.UI.Activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aait.zubtclient.App.Constant;
import com.aait.zubtclient.Base.ParentActivity;
import com.aait.zubtclient.Listeners.OnItemClickListener;
import com.aait.zubtclient.Models.BaseResponse;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Adapters.ImagesAdapter;
import com.aait.zubtclient.Uitls.CommonUtil;
import com.aait.zubtclient.Uitls.PermissionUtils;
import com.aait.zubtclient.Uitls.ProgressRequestBody;
import com.fxn.pix.Pix;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.zubtclient.App.Constant.RequestPermission.REQUEST_IMAGES;

public class SendRequestActivity extends ParentActivity implements OnItemClickListener,ProgressRequestBody.UploadCallbacks {
    String id;
    String mAdresse="", mLang=null, mLat = null, mAddress="",address = "";

    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @OnClick(R.id.notification)
    void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }
    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.request_location)
    TextView request_location;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.time)
    TextView time;
    private int day;
    private int month;
    private int year;
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;
    String ImageBasePath1 = null;
    String ImageBasePath2 = null;
    ArrayList<String> images = new ArrayList<>();
    @BindView(R.id.photos)
    RecyclerView photos;
    LinearLayoutManager linearLayoutManager;
    ArrayList<String > image = new ArrayList<>();
    ImagesAdapter imagesAdapter;
    @BindView(R.id.photo_lay)
    LinearLayout photo_lay;
    @BindView(R.id.add)
    ImageView add;

    @BindView(R.id.request_description)
    EditText request_description;
    @BindView(R.id.filter)
    ImageView filter;
    @Override
    protected void initializeComponents() {
        filter.setVisibility(View.GONE);
        id = getIntent().getStringExtra("id");
        act_title.setText(getString(R.string.send_request));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false);
        imagesAdapter = new ImagesAdapter(mContext,image,R.layout.recycler_images);
        imagesAdapter.setOnItemClickListener(this);
        photos.setLayoutManager(linearLayoutManager);
        photos.setAdapter(imagesAdapter);

    }
    @OnClick(R.id.request_location)
    void onLocation(){
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_send_request;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.date)
    void onDate(){
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog.OnDateSetListener listener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                date.setText(dayOfMonth + "-" + (monthOfYear+1) + "-" + year);
            }};

        DatePickerDialog dialog =
                new DatePickerDialog(this, listener, mYear, mMonth, mDay);
        dialog.show();
//        DatePickerDialog.OnDateSetListener listener=new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
//            {
//                date.setText(dayOfMonth + "-" + (monthOfYear+1) + "-" + year);
//            }};
//        DatePickerDialog dpDialog=new DatePickerDialog(mContext, listener, year, month, day);
//        dpDialog.show();
    }
    @OnClick(R.id.time)
    void onTime(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                time.setText( selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, false);
        //Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mAddress = data.getStringExtra("LOCATION");
                    address = data.getStringExtra("LOC");
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse+"  "+mAddress+"  "+address);
                    if (mLanguagePrefManager.getAppLanguage().equals("ar")) {
                        request_location.setText(mAddress);
                    }else if (mLanguagePrefManager.getAppLanguage().equals("en")){
                        request_location.setText(mAdresse);
                    }

                }
            }else if (requestCode == Constant.RequestPermission.REQUEST_IMAGES){
                ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                for (int i =0;i<returnValue.size();i++){
                    images.add(returnValue.get(i));
                }
                imagesAdapter.updateAll(images);
            }
        }
    }
    @OnClick(R.id.add_image)
    void onAddClick(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, REQUEST_IMAGES, 10);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, REQUEST_IMAGES, 10);
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        for (int i =0;i<ImageList.size();i++){
                            images.add(ImageList.get(i).getPath());
                        }

                        ImageBasePath = ImageList.get(0).getPath();
                        imagesAdapter.updateAll(images);
                        //civProfilePic.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.add_image)
                .setSelectMaxCount(10)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_image)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }


    @OnClick(R.id.add)
    void onAdd(){
        getPickImageWithPermission();
    }

    @Override
    public void onItemClick(View view, int position) {
        imagesAdapter.Delete(position);
        images.remove(position);
        imagesAdapter.notifyDataSetChanged();

    }
    private void sendRequest(ArrayList<String > paths){
        List<MultipartBody.Part> img = new ArrayList<>();
        showProgressDialog(getString(R.string.please_wait));
        for (String photo :paths) {
            MultipartBody.Part filePart = null;
            File ImageFile = new File(photo);
            ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, SendRequestActivity.this);
            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.getName(), fileBody);
            img.add(filePart);
        }
        RetroWeb.getClient().create(ServiceApi.class).addOrder(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token(),id,mAddress,mAdresse,mLat,mLang,request_description.getText().toString(),date.getText().toString(),time.getText().toString(),img).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        startActivity(new Intent(mContext,BackToHomeActivity.class));
                        SendRequestActivity.this.finish();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }

            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    @OnClick(R.id.request_price)
    void onRequest(){
        if (CommonUtil.checkTextError(request_location,getString(R.string.enter_request_location))||
        CommonUtil.checkTextError(date,getString(R.string.enter_date))||
        CommonUtil.checkTextError(time,getString(R.string.enter_time))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,request_description,getString(R.string.request_description))){
            return;
        }else {
            if (images.size()==0){
                CommonUtil.makeToast(mContext,getString(R.string.choose));
            }else {
                sendRequest(images);
            }
        }
    }
}
