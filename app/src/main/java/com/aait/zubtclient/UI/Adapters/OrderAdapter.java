package com.aait.zubtclient.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aait.zubtclient.Base.ParentRecyclerAdapter;
import com.aait.zubtclient.Base.ParentRecyclerViewHolder;
import com.aait.zubtclient.Models.OrderModel;
import com.aait.zubtclient.Models.ProviderModel;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Activities.OrderDetailsActivity;
import com.aait.zubtclient.UI.Activities.ProviderDetailsActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class OrderAdapter extends ParentRecyclerAdapter<OrderModel> {
    public OrderAdapter(Context context, List<OrderModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, viewGroup, false);
        OrderAdapter.ViewHolder holder = new OrderAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder parentRecyclerViewHolder, int i) {
        final OrderAdapter.ViewHolder viewHolder = (OrderAdapter.ViewHolder) parentRecyclerViewHolder;
        final OrderModel categoryModel = data.get(i);
        viewHolder.name.setText(categoryModel.getName());
        Glide.with(mcontext).load(categoryModel.getAvatar()).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter()).into(viewHolder.image);
        viewHolder.location.setText(categoryModel.getLocation());
        viewHolder.time.setText(categoryModel.getCreated_at()+"");

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent intent = new Intent(mcontext, OrderDetailsActivity.class);
                    intent.putExtra("id", categoryModel.getId() + "");
                    mcontext.startActivity(intent);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.photo)
        CircleImageView image;

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.location)
        TextView location;





        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
