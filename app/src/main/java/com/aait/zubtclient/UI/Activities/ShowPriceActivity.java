package com.aait.zubtclient.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.zubtclient.Base.ParentActivity;
import com.aait.zubtclient.Models.BaseResponse;
import com.aait.zubtclient.Models.OrderDetailsModel;
import com.aait.zubtclient.Models.OrderDetailsResponse;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowPriceActivity extends ParentActivity {
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @OnClick(R.id.notification)
    void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }
    String id;
    @BindView(R.id.price_added)
     TextView price_added;
    @BindView(R.id.material)
            TextView material;
    @BindView(R.id.price)
            TextView price;
    OrderDetailsModel orderDetailsModel;
    @BindView(R.id.filter)
    ImageView filter;
    @Override
    protected void initializeComponents() {
        filter.setVisibility(View.GONE);
        act_title.setText(getString(R.string.show_price));
        id = getIntent().getStringExtra("id");
        Log.e("id",id);
        getOrder();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_show_price;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    void SetData(OrderDetailsModel orderDetailsModel){
        if (orderDetailsModel.getInclude_material()==0){
            material.setVisibility(View.GONE);
        }else {
            material.setVisibility(View.VISIBLE);
        }
        if (orderDetailsModel.getInclude_tax()==0){
            price_added.setVisibility(View.GONE);
        }else {
            price_added.setVisibility(View.VISIBLE);
        }
        price.setText(orderDetailsModel.getCost()+getString(R.string.reyal));

    }
    private void getOrder(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getOrder(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token(),id).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        orderDetailsModel = response.body().getData();
                        SetData(orderDetailsModel);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.accept)
    void onAccept(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).accept(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token(),id).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        Intent intent = new Intent(mContext,OrderDetailsActivity.class);
                        intent.putExtra("id",id);
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.refuse)
    void onRefuse(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).refuse(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token(),
                id).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        startActivity(new Intent(mContext,MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
