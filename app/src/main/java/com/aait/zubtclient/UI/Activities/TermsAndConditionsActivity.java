package com.aait.zubtclient.UI.Activities;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.zubtclient.Base.ParentActivity;
import com.aait.zubtclient.Models.TermsResponse;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsAndConditionsActivity extends ParentActivity {
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.notification)
    void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.terms)
    TextView terms;
    @BindView(R.id.filter)
    ImageView filter;
    String type;
    @Override
    protected void initializeComponents() {
        filter.setVisibility(View.GONE);
        act_title.setText(getString(R.string.terms));
        type = getIntent().getStringExtra("type");
        if (type.equals("1")){
            notification.setVisibility(View.GONE);
        }else if (type.equals("2")){
            notification.setVisibility(View.VISIBLE);
        }
        getTerms();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_terms_and_conditions;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getTerms(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getTerms(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<TermsResponse>() {
            @Override
            public void onResponse(Call<TermsResponse> call, Response<TermsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        terms.setText(response.body().getData().getTerms());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<TermsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
