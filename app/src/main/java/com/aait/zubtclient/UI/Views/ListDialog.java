package com.aait.zubtclient.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.zubtclient.Listeners.OnItemClickListener;
import com.aait.zubtclient.Models.CityModel;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Adapters.ListDialogAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListDialog extends Dialog {

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.lay_no_data)
    LinearLayout lay_no_data;

    @BindView(R.id.tv_title)
    TextView tv_title;

    LinearLayoutManager mLinearLayoutManager;

    Context mContext;

    OnItemClickListener onItemClickListener;

    List<CityModel> mCarsList;

    ListDialogAdapter mListAdapter;

    String title;



    public ListDialog(Context mContext, OnItemClickListener onItemClickListener, List<CityModel> listModels,
                      String title) {
        super(mContext);
        this.mContext = mContext;
        this.mCarsList = listModels;
        this.onItemClickListener = onItemClickListener;
        this.title = title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dailog_custom_layout);
        getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        WindowManager.LayoutParams wmlp = getWindow().getAttributes();
        wmlp.gravity=(Gravity.TOP|Gravity.START);
        wmlp.x = 70;
        wmlp.y = 50;
        setCancelable(true);
        ButterKnife.bind(this);
        initializeComponents();
    }

    private void initializeComponents() {
        tv_title.setText(title);
        mLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(mLinearLayoutManager);
        mListAdapter = new ListDialogAdapter(mContext, mCarsList);
        mListAdapter.setOnItemClickListener(onItemClickListener);
        rvRecycle.setAdapter(mListAdapter);

        if (mCarsList.size() == 0) {
            lay_no_data.setVisibility(View.VISIBLE);
        }
    }


}

