package com.aait.zubtclient.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aait.zubtclient.Models.BaseResponse;
import com.aait.zubtclient.Models.FinishModel;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.Pereferences.LanguagePrefManager;
import com.aait.zubtclient.Pereferences.SharedPrefManager;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Activities.MainActivity;
import com.aait.zubtclient.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RateDialog extends Dialog {
    FinishModel finishModel;
    Context mContext;
    SharedPrefManager mSharedPrefManager;
    LanguagePrefManager mLanguagePrefManager;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.rating)
    RatingBar ratingBar;
    @BindView(R.id.write_comment)
    EditText write_comment;
    int count = 0;
    public RateDialog(@NonNull Context context,FinishModel finishModel) {
        super(context);
        this.mContext=context;
        this.finishModel = finishModel;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dailog_rate);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        mSharedPrefManager = new SharedPrefManager(mContext);
        mLanguagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {
        Glide.with(mContext).load(finishModel.getAvatar()).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter()).into(image);
        name.setText(finishModel.getName());

    }
    @OnClick(R.id.rate)
    void onRate(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,write_comment,mContext.getString(R.string.write_comment))){
            return;
        }else {
            if (count>0){

            }else {
                rate();
            }
            count++;
        }
    }
    private void rate(){
        RetroWeb.getClient().create(ServiceApi.class).addReview(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token(),(int)ratingBar.getRating(),write_comment.getText().toString(),finishModel.getId()).enqueue(
                new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if (response.isSuccessful()){
                            if (response.body().getValue().equals("1")){
                                mContext.startActivity(new Intent(mContext, MainActivity.class));
                            }else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext,t);
                        t.printStackTrace();

                    }
                }
        );
    }
}
