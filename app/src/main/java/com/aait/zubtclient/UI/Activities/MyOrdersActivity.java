package com.aait.zubtclient.UI.Activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.zubtclient.Base.ParentActivity;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Adapters.SubscribeTapAdapter;
import com.aait.zubtclient.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class MyOrdersActivity extends ParentActivity {
    @BindView(R.id.myOrders)
    TabLayout myOrdersTab;

    @BindView(R.id.myOrdersViewPager)
    ViewPager myOrdersViewPager;
    @OnClick(R.id.notification)
    void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    private SubscribeTapAdapter mAdapter;
    @BindView(R.id.filter)
    ImageView filter;
    @Override
    protected void initializeComponents() {
        filter.setVisibility(View.GONE);
        act_title.setText(getString(R.string.my_orders));
        mAdapter = new SubscribeTapAdapter(mContext,getSupportFragmentManager());
        myOrdersViewPager.setAdapter(mAdapter);
        myOrdersTab.setupWithViewPager(myOrdersViewPager);


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_my_orders;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
}
