package com.aait.zubtclient.UI.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.zubtclient.App.Constant;
import com.aait.zubtclient.Base.ParentActivity;
import com.aait.zubtclient.Models.LoginResponse;
import com.aait.zubtclient.Models.UserModel;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Views.ChangePasswordDiolg;
import com.aait.zubtclient.Uitls.CommonUtil;
import com.aait.zubtclient.Uitls.PermissionUtils;
import com.aait.zubtclient.Uitls.ProgressRequestBody;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Pix;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.zubtclient.App.Constant.RequestPermission.REQUEST_IMAGES;

public class ProfileActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks {
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    ChangePasswordDiolg changePasswordDiolg;
    @OnClick(R.id.change_pass)
    void onChangePass(){
        changePasswordDiolg = new ChangePasswordDiolg(mContext);
        changePasswordDiolg.show();
    }
    @OnClick(R.id.notification)
    void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.user_name)
    EditText user_name;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.civ_user_image)
    CircleImageView civ_user_image;
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;
    @BindView(R.id.filter)
    ImageView filter;
    @Override
    protected void initializeComponents() {
        filter.setVisibility(View.GONE);
        act_title.setText(getString(R.string.profile));
        getProfile();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    void SetData(UserModel userModel){
        Glide.with(mContext).load(userModel.getAvatar()).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter()).into(civ_user_image);
        name.setText(userModel.getName());
        user_name.setText(userModel.getName());
        phone.setText(userModel.getPhone());
        email.setText(userModel.getEmail());

    }
    @OnClick(R.id.civ_user_image)
    void onImage(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, REQUEST_IMAGES, 1);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, REQUEST_IMAGES, 1);
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        civ_user_image.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle("avatar")
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_image)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }

    private void getProfile(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProfile(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token()).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        SetData(response.body().getData());
                        mSharedPrefManager.setUserData(response.body().getData());


                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void editWithAvatar(String path){
        showProgressDialog(getResources().getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ProfileActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).editWithAvatar(mSharedPrefManager.getUserData().getApi_token(),mLanguagePrefManager
        .getAppLanguage(),user_name.getText().toString(),phone.getText().toString(),email.getText().toString(),filePart).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        SetData(response.body().getData());
                        mSharedPrefManager.setUserData(response.body().getData());
                        CommonUtil.makeToast(mContext,getString(R.string.your_data_changed));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void edit(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).edit(mSharedPrefManager.getUserData().getApi_token(),mLanguagePrefManager.getAppLanguage(),user_name.getText().toString(),phone.getText().toString(),email.getText().toString()).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        SetData(response.body().getData());
                        mSharedPrefManager.setUserData(response.body().getData());
                        CommonUtil.makeToast(mContext,getString(R.string.your_data_changed));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.save)
    void onSave(){
        if (ImageBasePath != null){
            if(CommonUtil.checkTextError((AppCompatActivity)mContext,user_name,getString(R.string.enter_user_name))||
            CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.enter_email))||
                    ! CommonUtil.isEmailValid(email,getString(R.string.correct_email))){
                return;
            }else {
                editWithAvatar(ImageBasePath);
            }
        }else {
            if(CommonUtil.checkTextError((AppCompatActivity)mContext,user_name,getString(R.string.enter_user_name))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.enter_email))||
                    ! CommonUtil.isEmailValid(email,getString(R.string.correct_email))){
                return;
            }else {
                edit();
            }
        }
    }
    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null) {
             if (requestCode == Constant.RequestPermission.REQUEST_IMAGES){
                ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath =returnValue.get(0);
                 civ_user_image.setImageURI(Uri.parse(ImageBasePath));
            }
        }
    }
}
