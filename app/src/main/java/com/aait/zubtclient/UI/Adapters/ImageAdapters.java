package com.aait.zubtclient.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aait.zubtclient.Base.ParentRecyclerAdapter;
import com.aait.zubtclient.Base.ParentRecyclerViewHolder;
import com.aait.zubtclient.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;

public class ImageAdapters extends ParentRecyclerAdapter<String> {
    public ImageAdapters(Context context, List<String> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, viewGroup, false);
        ImageAdapters.ViewHolder holder = new ImageAdapters.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder parentRecyclerViewHolder, final int i) {
        final ImageAdapters.ViewHolder viewHolder = (ImageAdapters.ViewHolder) parentRecyclerViewHolder;
        final String categoryModel = data.get(i);

        Glide.with(mcontext).load(categoryModel).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter()).into(viewHolder.image);

        viewHolder.delete.setVisibility(View.GONE);
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.delete)
        ImageView delete;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
