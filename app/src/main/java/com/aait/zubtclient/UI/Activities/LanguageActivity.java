package com.aait.zubtclient.UI.Activities;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.zubtclient.Base.ParentActivity;
import com.aait.zubtclient.Models.LoginResponse;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LanguageActivity extends ParentActivity {
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.notification)
    void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }
    @BindView(R.id.filter)
    ImageView filter;
    @Override
    protected void initializeComponents() {
        filter.setVisibility(View.GONE);
        act_title.setText(getString(R.string.app_lang));

    }
    @OnClick(R.id.arabic)
    void onArabic(){
        if (mSharedPrefManager.getLoginStatus()){
            lang("ar");
        }else {
            mLanguagePrefManager.setAppLanguage("ar");
            startActivity(new Intent(mContext, SplashActivity.class));
        }

    }
    @OnClick(R.id.english)
    void onEnglish(){
        if (mSharedPrefManager.getLoginStatus()){
            lang("en");
        }else {
            mLanguagePrefManager.setAppLanguage("en");
            startActivity(new Intent(mContext, SplashActivity.class));
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_language;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void lang(final String lang){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).lang(mSharedPrefManager.getUserData().getApi_token(),lang).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        mLanguagePrefManager.setAppLanguage(lang);
                        startActivity(new Intent(mContext,SplashActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
