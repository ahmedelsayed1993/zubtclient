package com.aait.zubtclient.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.aait.zubtclient.Base.ParentRecyclerAdapter;
import com.aait.zubtclient.Base.ParentRecyclerViewHolder;
import com.aait.zubtclient.Models.CategoryModel;
import com.aait.zubtclient.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;

public class CategoryAdapters extends ParentRecyclerAdapter<CategoryModel> {
    public CategoryAdapters(Context context, List<CategoryModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        CategoryAdapters.ViewHolder holder = new CategoryAdapters.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final CategoryAdapters.ViewHolder viewHolder = (CategoryAdapters.ViewHolder) holder;
        final CategoryModel categoryModel = data.get(position);
        viewHolder.cat.setText(categoryModel.getValue());
        Glide.with(mcontext).load(categoryModel.getImage()).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter()).into(viewHolder.icon);
        viewHolder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.image)
        ImageView icon;

        @BindView(R.id.name)
        TextView cat;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
