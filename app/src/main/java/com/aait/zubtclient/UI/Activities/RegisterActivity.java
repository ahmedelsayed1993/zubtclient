package com.aait.zubtclient.UI.Activities;

/**
 * created by ahmed el_sayed  8/10/2019
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.aait.zubtclient.Base.ParentActivity;
import com.aait.zubtclient.Models.RegisterResponse;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.Uitls.CommonUtil;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends ParentActivity {
    @BindView(R.id.user_name)
    EditText user_name;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.check_conditions)
    CheckBox check_conditions;
    @BindView(R.id.terms)
    TextView terms;
    @OnClick(R.id.login)
    void onLogin(){
        startActivity(new Intent(mContext,LoginActivity.class));
    }
    @OnClick(R.id.terms)
    void onTerms(){
        Intent intent = new Intent(mContext,TermsAndConditionsActivity.class);
        intent.putExtra("type","1");
        startActivity(intent);
    }
    @OnClick(R.id.view)
    void onView(){

        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

    }
    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.register)
    void onRegister(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,user_name,getString(R.string.enter_user_name))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.enter_phone))||
        CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
        CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.enter_email))||
               ! CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,password,getString(R.string.enter_password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)
        ){
            return;

        }else {
            if (check_conditions.isChecked()){
                Register();
            }else {
                CommonUtil.makeToast(mContext,getString(R.string.check_terms));
            }
        }
    }

    private void Register(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).register(mLanguagePrefManager.getAppLanguage(),user_name.getText().toString(),phone.getText().toString(),email.getText().toString(),password.getText().toString(),"client").enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        Log.e("fail",new Gson().toJson(response.body().getData()));
                        Intent intent = new Intent(mContext,ActivateAccountActivity.class);
                        intent.putExtra("token",response.body().getData().getApi_token());
                        intent.putExtra("code",response.body().getData().getCode());
                        startActivity(intent);
                        RegisterActivity.this.finish();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.e("fail",new Gson().toJson(t));
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

}
