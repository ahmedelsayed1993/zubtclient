package com.aait.zubtclient.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.aait.zubtclient.Base.BaseFragment;
import com.aait.zubtclient.Listeners.DrawerListner;
import com.aait.zubtclient.Listeners.OnItemClickListener;
import com.aait.zubtclient.Models.BaseResponse;
import com.aait.zubtclient.Models.NavigationModel;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Activities.AboutAppActivity;
import com.aait.zubtclient.UI.Activities.ContactUsActivity;
import com.aait.zubtclient.UI.Activities.FavouriteActivity;
import com.aait.zubtclient.UI.Activities.LanguageActivity;
import com.aait.zubtclient.UI.Activities.LoginActivity;
import com.aait.zubtclient.UI.Activities.MainActivity;
import com.aait.zubtclient.UI.Activities.MyOrdersActivity;
import com.aait.zubtclient.UI.Activities.ProfileActivity;
import com.aait.zubtclient.UI.Activities.RepeatedQuestionActivity;
import com.aait.zubtclient.UI.Activities.SplashActivity;
import com.aait.zubtclient.UI.Activities.TermsAndConditionsActivity;
import com.aait.zubtclient.UI.Adapters.NavigationDrawerAdapter;
import com.aait.zubtclient.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */

public class NavigationFragment extends BaseFragment implements OnItemClickListener {


    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.lay_profile)
    LinearLayout lay_profile;



    @BindView(R.id.civ_user_image)
    ImageView civ_user_image;
    @BindView(R.id.civ_user_name)
    TextView civ_user_name;


    ArrayList<NavigationModel> mNavigationModels;

    NavigationDrawerAdapter drawerAdapter;

    DrawerListner drawerListner;

    private AppCompatActivity activity;
    String newToken= null;

    public static NavigationFragment newInstance() {
        Bundle args = new Bundle();
        NavigationFragment fragment = new NavigationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_navigation_menu;
    }

    @Override
    protected void initializeComponents(final View view) {
        activity = (AppCompatActivity) (NavigationFragment.this).getActivity();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( getActivity(),  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });


        setNavData();
        setMenuData();
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(final View view, final int position) {

        if (mSharedPrefManager.getLoginStatus()) {
            switch (position) {
                case 0:
                    startActivity(new Intent(mContext, MainActivity.class));
                    break;
                case 1:
                    startActivity(new Intent(mContext, ProfileActivity.class));
                    break;
                case 2:
                    startActivity(new Intent(mContext, MyOrdersActivity.class));
                    break;
                case 3:
                    startActivity(new Intent(mContext, FavouriteActivity.class));
                    break;
                case 4:
                    startActivity(new Intent(mContext, LanguageActivity.class));
                    break;
                case 5:
                    startActivity(new Intent(mContext, AboutAppActivity.class));
                    break;
                case 6:
                    Intent intent = new Intent(mContext,TermsAndConditionsActivity.class);
                    intent.putExtra("type","2");
                    startActivity(intent);
                    break;
                case 7:
                    startActivity(new Intent(mContext, RepeatedQuestionActivity.class));
                    break;
                case 8:
                    startActivity(new Intent(mContext, ContactUsActivity.class));
                    break;
                case 9:
                    LogOut();
                    break;


            }
        }else {
            switch (position) {
                case 0:
                    startActivity(new Intent(mContext, MainActivity.class));
                    break;
                case 1:
                    CommonUtil.makeToast(mContext,getString(R.string.must_login));
                    break;
                case 2:
                    CommonUtil.makeToast(mContext,getString(R.string.must_login));
                    break;
                case 3:
                    CommonUtil.makeToast(mContext,getString(R.string.must_login));
                    break;
                case 4:
                    startActivity(new Intent(mContext, LanguageActivity.class));
                    break;
                case 5:
                    startActivity(new Intent(mContext, AboutAppActivity.class));
                    break;
                case 6:
                    Intent intent = new Intent(mContext,TermsAndConditionsActivity.class);
                    intent.putExtra("type","2");
                    startActivity(intent);
                    break;
                case 7:
                    startActivity(new Intent(mContext, RepeatedQuestionActivity.class));
                    break;
                case 8:
                    startActivity(new Intent(mContext, ContactUsActivity.class));
                    break;
                case 9:
                    startActivity(new Intent(mContext,LoginActivity.class));
                    break;


            }
        }
        drawerListner.OpenCloseDrawer();
    }


    public void setDrawerListner(DrawerListner drawerListner) {
        this.drawerListner = drawerListner;
    }

    public void setMenuData() {
        mNavigationModels = new ArrayList<>();
if (mSharedPrefManager.getLoginStatus()) {
    mNavigationModels.add(new NavigationModel(getString(R.string.home), R.mipmap.noun_home));
    mNavigationModels.add(new NavigationModel(getString(R.string.profile), R.mipmap.noun_profile));
    mNavigationModels.add(new NavigationModel(getString(R.string.my_orders), R.mipmap.noun_order));
    mNavigationModels.add(new NavigationModel(getString(R.string.favourite), R.mipmap.likeee));
    mNavigationModels.add(new NavigationModel(getString(R.string.language), R.mipmap.language));
    mNavigationModels.add(new NavigationModel(getString(R.string.about_app), R.mipmap.noun_info));
    mNavigationModels.add(new NavigationModel(getString(R.string.terms), R.mipmap.noun_policy));
    mNavigationModels.add(new NavigationModel(getString(R.string.repeated_questions), R.mipmap.noun_faq));
    mNavigationModels.add(new NavigationModel(getString(R.string.contact_us), R.mipmap.noun_contact));
    mNavigationModels.add(new NavigationModel(getString(R.string.logout), R.mipmap.signout));
}else {
    mNavigationModels.add(new NavigationModel(getString(R.string.home), R.mipmap.noun_home));
    mNavigationModels.add(new NavigationModel(getString(R.string.profile), R.mipmap.noun_profile));
    mNavigationModels.add(new NavigationModel(getString(R.string.my_orders), R.mipmap.noun_order));
    mNavigationModels.add(new NavigationModel(getString(R.string.favourite), R.mipmap.likeee));
    mNavigationModels.add(new NavigationModel(getString(R.string.language), R.mipmap.language));
    mNavigationModels.add(new NavigationModel(getString(R.string.about_app), R.mipmap.noun_info));
    mNavigationModels.add(new NavigationModel(getString(R.string.terms), R.mipmap.noun_policy));
    mNavigationModels.add(new NavigationModel(getString(R.string.repeated_questions), R.mipmap.noun_faq));
    mNavigationModels.add(new NavigationModel(getString(R.string.contact_us), R.mipmap.noun_contact));
    mNavigationModels.add(new NavigationModel(getString(R.string.login), R.mipmap.signout));
}



        rvRecycle.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        drawerAdapter = new NavigationDrawerAdapter(mContext, mNavigationModels,
                R.layout.recycle_navigation_row);
        drawerAdapter.setOnItemClickListener(this);
        rvRecycle.setAdapter(drawerAdapter);
    }


    public void setNavData() {
        if (mSharedPrefManager.getLoginStatus()) {
            Glide.with(mContext).load(mSharedPrefManager.getUserData().getAvatar()).apply(new RequestOptions().placeholder(R.mipmap.camera).fitCenter())
             .into(civ_user_image);
            civ_user_name.setText(mSharedPrefManager.getUserData().getName());
        }else {

                    }
    }
    private void LogOut(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).logout(mSharedPrefManager.getUserData().getApi_token(),newToken).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        mSharedPrefManager.setLoginStatus(false);
                        mSharedPrefManager.Logout();
                        startActivity(new Intent(mContext, SplashActivity.class));

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        mSharedPrefManager.setLoginStatus(false);
                        mSharedPrefManager.Logout();
                        startActivity(new Intent(mContext, SplashActivity.class));
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }

}
