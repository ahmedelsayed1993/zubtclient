package com.aait.zubtclient.UI.Activities;
/**
 * created by ahmed el_sayed  8/10/2019
 */

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aait.zubtclient.Base.ParentActivity;
import com.aait.zubtclient.Listeners.DrawerListner;
import com.aait.zubtclient.Listeners.OnItemClickListener;
import com.aait.zubtclient.Models.CategoryModel;
import com.aait.zubtclient.Models.HomeModel;
import com.aait.zubtclient.Models.HomeResponse;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Adapters.CategoryAdapters;
import com.aait.zubtclient.UI.Adapters.RecyclerPoupupAds;
import com.aait.zubtclient.UI.Fragments.NavigationFragment;
import com.aait.zubtclient.Uitls.CommonUtil;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.gson.Gson;
import com.pixelcan.inkpageindicator.InkPageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends ParentActivity implements DrawerListner, OnItemClickListener, ViewPagerEx.OnPageChangeListener, BaseSliderView.OnSliderClickListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.nav_view)
    FrameLayout navView;

    @BindView(R.id.app_bar)
    AppBarLayout app_bar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ads_lay)
    CardView images;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;

    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    RecyclerPoupupAds recyclerPoupupAds;
    @OnClick(R.id.notification)
    void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }

    @BindView(R.id.iv_menu)
    ImageView ivMenu;

    @BindView(R.id.tv_title)
    TextView tvTitle;
    NavigationFragment mNavigationFragment;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    GridLayoutManager gridLayoutManager;
    CategoryAdapters categoryAdapters;
    ArrayList<CategoryModel> categoryModels = new ArrayList<>();
    ArrayList<String> banners = new ArrayList<>();
    @Override
    protected void initializeComponents() {
        mNavigationFragment = NavigationFragment.newInstance();
        mNavigationFragment.setDrawerListner(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_view, mNavigationFragment).commit();
        gridLayoutManager = new GridLayoutManager(mContext,2);
        categoryAdapters = new CategoryAdapters(mContext,categoryModels,R.layout.recycler_catergory);
        categoryAdapters.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(gridLayoutManager);
        rvRecycle.setAdapter(categoryAdapters);


        if (mSharedPrefManager.getLoginStatus()) {
            getHome(mSharedPrefManager.getUserData().getApi_token());
        } else {
            getHome(null);
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void OpenCloseDrawer() {
        if (drawerLayout != null) {
            if (mLanguagePrefManager.getAppLanguage().equals("en")) {
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            } else {
                if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        }
    }
    void setData(HomeModel homeModel){
        categoryModels = homeModel.getCategories();
        banners = homeModel.getBanners();


        recyclerPoupupAds = new RecyclerPoupupAds(mContext,banners);
//        for (String image :
//                   banners) {
//                // sliderView = new DefaultSliderView(this);
////                viewPager.
////                        image(image)
////                        .setScaleType(BaseSliderView.ScaleType.Fit)
////                        .setOnSliderClickListener(this);
////                images.removeView((View) viewPager.getParent());
////                viewPager.addView((View) viewPager.getParent());
//              //  viewPager.addView(ViewPagerEx.inflate(mContext,R.layout.recycler_image,viewPager));
//
//            }
//        viewPager.arrowScroll(2500);
//        indicator.setViewPager(viewPager);
//
//            mSlider.setPresetTransformer(SliderLayout.Transformer.Default);
//            //     mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
//            mSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
//
//            mSlider.setCustomAnimation(new DescriptionAnimation());
//            mSlider.setDuration(10000);
//            mSlider.addOnPageChangeListener(this);
//
//        }
        viewPager.setAdapter(recyclerPoupupAds);

        indicator.setViewPager(viewPager);


        NUM_PAGES = recyclerPoupupAds.getCount();
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == NUM_PAGES){ currentPage = 0; }
                viewPager.setCurrentItem(currentPage,false);
                currentPage++;

            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);


            }
        }, 5000,5000);
//             setSlider(banners);

        if (homeModel.getCategories().size()==0){

        }else {

            categoryAdapters.updateAll(homeModel.getCategories());
        }
    }
    @OnClick(R.id.iv_menu)
    void onMenuClick() {
        this.OpenCloseDrawer();
    }
    private void getHome(String token){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getHome(mLanguagePrefManager.getAppLanguage(),token).enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, final Response<HomeResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                       setData(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                Log.e("sss", new Gson().toJson(t));
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });

    }
//    private void setSlider(List<String> images) {
//
//        if (images.size() > 0) {
//
//            for (String image :
//                    images) {
//                DefaultSliderView sliderView = new DefaultSliderView(this);
//                sliderView.
//                        image(image)
//                        .setScaleType(BaseSliderView.ScaleType.Fit)
//                        .setOnSliderClickListener(this);
//                viewPager.addSlider(sliderView);
//            }
//
//            viewPager.setPresetTransformer(SliderLayout.Transformer.Default);
//            //     mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
//            viewPager.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
//
//            viewPager.setCustomAnimation(new DescriptionAnimation());
//            viewPager.setDuration(10000);
//            viewPager.addOnPageChangeListener(this);
//
//        }
//
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent =new Intent(mContext,ProvidersActivity.class);
        intent.putExtra("cat",categoryModels.get(position));
        startActivity(intent);

    }



    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
