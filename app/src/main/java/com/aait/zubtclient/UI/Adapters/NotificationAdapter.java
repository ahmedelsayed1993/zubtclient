package com.aait.zubtclient.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.zubtclient.Base.ParentRecyclerAdapter;
import com.aait.zubtclient.Base.ParentRecyclerViewHolder;
import com.aait.zubtclient.Models.BaseResponse;
import com.aait.zubtclient.Models.NotificationModel;
import com.aait.zubtclient.Models.OrderModel;
import com.aait.zubtclient.Network.RetroWeb;
import com.aait.zubtclient.Network.ServiceApi;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Activities.MainActivity;
import com.aait.zubtclient.UI.Activities.OrderDetailsActivity;
import com.aait.zubtclient.UI.Activities.ShowPriceActivity;
import com.aait.zubtclient.UI.Views.NotifyDailog;
import com.aait.zubtclient.Uitls.CommonUtil;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationAdapter extends ParentRecyclerAdapter<NotificationModel> {
    NotifyDailog notifyDailog;
    public NotificationAdapter(Context context, List<NotificationModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, viewGroup, false);
        NotificationAdapter.ViewHolder holder = new NotificationAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder parentRecyclerViewHolder, int i) {
        final NotificationAdapter.ViewHolder viewHolder = (NotificationAdapter.ViewHolder) parentRecyclerViewHolder;
        final NotificationModel categoryModel = data.get(i);
        viewHolder.text.setText(categoryModel.getData());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (categoryModel.getType().equals("provider_accepted")){
                    Intent intent = new Intent(mcontext, ShowPriceActivity.class);
                    intent.putExtra("id",categoryModel.getOrder_id()+"");
                    mcontext.startActivity(intent);
                }
               else if (categoryModel.getType().equals("dashboard")&&!categoryModel.getOrder_id().equals("")){
                    Intent intent = new Intent(mcontext, OrderDetailsActivity.class);
                    intent.putExtra("id",categoryModel.getOrder_id()+"");
                    mcontext.startActivity(intent);
                }else if (categoryModel.getType().equals("confirm_finish")){
                    Intent intent = new Intent(mcontext, OrderDetailsActivity.class);
                    intent.putExtra("id",categoryModel.getOrder_id()+"");
                    mcontext.startActivity(intent);
                }else if (categoryModel.getType().equals("dashboard")&&categoryModel.getOrder_id().equals("")){
                   notifyDailog = new NotifyDailog(mcontext,categoryModel.getData());
                   notifyDailog.show();
                }
            }
        });
        viewHolder.delet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RetroWeb.getClient().create(ServiceApi.class).remove(mSharedPrefManager.getUserData().getApi_token(),categoryModel.getId()).enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if (response.isSuccessful()){
                            if (response.body().getValue().equals("1")){
                                mcontext.startActivity(new Intent(mcontext, MainActivity.class));
                            }else {
                                CommonUtil.makeToast(mcontext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        CommonUtil.handleException(mcontext,t);
                        t.printStackTrace();


                    }
                });
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.delete)
        ImageView delet;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
