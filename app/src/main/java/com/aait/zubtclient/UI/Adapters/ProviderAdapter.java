package com.aait.zubtclient.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aait.zubtclient.Base.ParentRecyclerAdapter;
import com.aait.zubtclient.Base.ParentRecyclerViewHolder;
import com.aait.zubtclient.Models.CategoryModel;
import com.aait.zubtclient.Models.ProviderModel;
import com.aait.zubtclient.R;
import com.aait.zubtclient.UI.Activities.ProviderDetailsActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;

public class ProviderAdapter extends ParentRecyclerAdapter<ProviderModel> {
    public ProviderAdapter(Context context, List<ProviderModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, viewGroup, false);
        ProviderAdapter.ViewHolder holder = new ProviderAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder parentRecyclerViewHolder, final int i) {
        final ProviderAdapter.ViewHolder viewHolder = (ProviderAdapter.ViewHolder) parentRecyclerViewHolder;
        final ProviderModel categoryModel = data.get(i);
        viewHolder.name.setText(categoryModel.getName());
        Glide.with(mcontext).load(categoryModel.getAvatar()).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter()).into(viewHolder.image);
        viewHolder.city.setText(categoryModel.getCity());
        viewHolder.rate.setText(categoryModel.getRate()+"");
        viewHolder.rating.setRating(Float.parseFloat(categoryModel.getRate()) );
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mcontext, ProviderDetailsActivity.class);
                intent.putExtra("id",categoryModel.getId()+"");
                mcontext.startActivity(intent);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.rating)
        RatingBar rating;
        @BindView(R.id.rate)
                TextView rate;
        @BindView(R.id.city)
                TextView city;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
