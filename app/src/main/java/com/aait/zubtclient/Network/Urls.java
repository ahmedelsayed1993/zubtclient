package com.aait.zubtclient.Network;

public class Urls {
    public final static String baseUrl = "https://zubt.aait-sa.com/api/";

    public final static String Terms = "about-and-terms";
    public final static String ContactInfo = "contact-info";
    public final static String COntactUs = "contact-us";
    public final static String Home = "client/home";
    public final static String Register = "register";
    public final static String Activate = "active-user";
    public final static String Login = "sign-in";
    public final static String FOrgotPassword = "forget-password";
    public final static String ResetPassword = "reset-password";
    public final static String Profile = "profile";
    public final static String LogOut = "logout";
    public final static String ChangePassword = "change-password";
    public final static String Repeated = "questions";
    public final static String Providers = "client/providers";
    public final static String ProviderInfo = "provider-info";
    public final static String AddFavourite = "client/toggle-favourite";
    public final static String AddOrder = "client/add-order";
    public final static String Orders = "client/orders";
    public final static String Favourite = "client/favourites";
    public final static String EditProfile = "edit-profile";
    public final static String Notification = "notifications";
    public final static String OrderInfo = "order-info";
    public final static String AcceptPrice = "client/accept-price";
    public final static String RefusePrice = "client/refuse-price";
    public final static String Finish = "client/confirm-finish";
    public final static String AddReview = "client/add-review";
    public final static String CancelOrder = "client/cancel-order";
    public final static String removeNotification = "remove-notification";
    public final static String ReportReview = "report-review";
    public final static String ReportProvider = "client/report-provider";


}
