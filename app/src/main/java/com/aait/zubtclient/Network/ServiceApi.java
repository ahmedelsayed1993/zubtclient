package com.aait.zubtclient.Network;

import com.aait.zubtclient.Models.BaseResponse;
import com.aait.zubtclient.Models.ContactInfoResponse;
import com.aait.zubtclient.Models.FavouriteResponse;
import com.aait.zubtclient.Models.FinishResponse;
import com.aait.zubtclient.Models.HomeResponse;
import com.aait.zubtclient.Models.LoginResponse;
import com.aait.zubtclient.Models.NotificationResponse;
import com.aait.zubtclient.Models.OrderDetailsResponse;
import com.aait.zubtclient.Models.OrdersResponse;
import com.aait.zubtclient.Models.ProviderDetialsResponse;
import com.aait.zubtclient.Models.ProvidersResponse;
import com.aait.zubtclient.Models.QuestionResponse;
import com.aait.zubtclient.Models.QuestionsResponse;
import com.aait.zubtclient.Models.RegisterResponse;
import com.aait.zubtclient.Models.TermsResponse;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ServiceApi {

    @GET(Urls.Terms)
    Call<TermsResponse> getTerms(@Header("lang") String lang);

    @POST(Urls.ContactInfo)
    Call<ContactInfoResponse> getContacts();

    @POST(Urls.COntactUs)
    Call<BaseResponse> Contact(@Header("lang") String lang,
                               @Query("name") String name,
                               @Query("email") String email,
                               @Query("message") String message);
    @POST(Urls.Home)
    Call<HomeResponse> getHome(@Header("lang") String lang,
                               @Header("token") String token);
    @POST(Urls.Register)
    Call<RegisterResponse> register(@Header("lang") String lang,
                                    @Query("name") String name,
                                    @Query("phone") String phone,
                                    @Query("email") String email,
                                    @Query("password") String password,
                                    @Query("user_type") String user_type);

    @POST(Urls.Activate)
    Call<BaseResponse> Activate(@Header("lang") String lang,
                                @Header("token") String token,
                                @Query("code") String code);

    @POST(Urls.Login)
    Call<LoginResponse> Login(@Header("lang") String lang,
                              @Query("phone") String phone,
                              @Query("password") String password,
                              @Query("device_id") String device_id,
                              @Query("device_type") String device_type,
                              @Query("user_type") String user_type);

    @POST(Urls.FOrgotPassword)
    Call<LoginResponse> forgotPass(@Header("lang") String lang,
                                   @Query("phone") String phone,
                                   @Query("user_type") String user_type);
    @POST(Urls.ResetPassword)
    Call<BaseResponse> resetPassword(@Header("lang") String lang,
                                     @Header("token") String token,
                                     @Query("password") String password,
                                     @Query("code") String code);

    @POST(Urls.Profile)
    Call<LoginResponse> getProfile(@Header("lang") String lang,
                                   @Header("token") String token);
    @POST(Urls.LogOut)
    Call<BaseResponse> logout(@Header("token") String token,
                              @Query("device_id") String device_id);
    @POST(Urls.ChangePassword)
    Call<BaseResponse> changePass(@Header("lang") String lang,
                                  @Header("token") String token,
                                  @Query("old_password") String old_password,
                                  @Query("password") String password);
    @GET(Urls.Repeated)
    Call<QuestionResponse> getQuestions(@Header("lang") String lang,
                                        @Query("page") int page);

    @POST(Urls.Providers)
    Call<ProvidersResponse> getProviders(@Header("lang") String lang,
                                         @Query("keyword") String keyword,
                                         @Query("category_id") String category_id,
                                         @Query("filter") String filter,
                                         @Query("lat") String lat,
                                         @Query("lng") String lng);

    @POST(Urls.ProviderInfo)
    Call<ProviderDetialsResponse> getProvider(@Header("lang") String lang,
                                              @Header("token") String token,
                                              @Query("provider_id") String provider_id);

    @POST(Urls.AddFavourite)
    Call<ProviderDetialsResponse> addFav(@Header("lang") String lang,
                                         @Header("token") String token,
                                         @Query("provider_id") String provider_id);
    @Multipart
    @POST(Urls.AddOrder)
    Call<BaseResponse> addOrder(@Header("lang") String lang,
                                @Header("token") String token,
                                @Query("provider_id") String provider_id,
                                @Query("location_ar") String location_ar,
                                @Query("location_en") String location_en,
                                @Query("lat") String lat,
                                @Query("lng") String lng,
                                @Query("details") String details,
                                @Query("date") String date,
                                @Query("time") String time,
                                @Part List<MultipartBody.Part> images);

    @POST(Urls.Orders)
    Call<OrdersResponse> getOrders(@Header("lang") String lang,
                                   @Header("token") String token,
                                   @Query("type") String type,
                                   @Query("page") int page );

    @POST(Urls.Favourite)
    Call<FavouriteResponse> getFavourite(@Header("lang") String lang,
                                         @Header("token") String token,
                                         @Query("page") int page,
                                         @Query("keyword") String keyword);

    @POST(Urls.EditProfile)
    Call<LoginResponse> edit(@Header("token") String token,
                             @Query("lang") String lang,
                             @Query("name") String name,
                             @Query("phone") String phone,
                             @Query("email") String email);
    @POST(Urls.EditProfile)
    Call<LoginResponse> lang(@Header("token") String token,
                             @Query("lang") String lang);

    @Multipart
    @POST(Urls.EditProfile)
    Call<LoginResponse> editWithAvatar(@Header("token") String token,
                                       @Query("lang") String lang,
                                       @Query("name") String name,
                                       @Query("phone") String phone,
                                       @Query("email") String email,
                                       @Part MultipartBody.Part avatar);
    @POST(Urls.Notification)
    Call<NotificationResponse> getNotification(@Header("lang") String lang,
                                               @Header("token") String token,
                                               @Query("page") int page);
    @POST(Urls.OrderInfo)
    Call<OrderDetailsResponse> getOrder(@Header("lang") String lang,
                                        @Header("token") String token,
                                        @Query("order_id") String order_id);
    @POST(Urls.AcceptPrice)
    Call<BaseResponse> accept(@Header("lang") String lang,
                              @Header("token") String token,
                              @Query("order_id") String order_id);
    @POST(Urls.CancelOrder)
    Call<BaseResponse> cancel(@Header("lang") String lang,
                              @Header("token") String token,
                              @Query("order_id") String order_id);
    @POST(Urls.Finish)
    Call<FinishResponse> finish(@Header("lang") String lang,
                                @Header("token") String token,
                                @Query("order_id") String order_id);
    @POST(Urls.RefusePrice)
    Call<BaseResponse> refuse(@Header("lang") String lang,
                              @Header("token") String token,
                              @Query("order_id") String order_id);
    @POST(Urls.AddReview)
    Call<BaseResponse> addReview(@Header("lang") String lang,
                                 @Header("token") String token,
                                 @Query("rate") int rate,
                                 @Query("comment") String comment,
                                 @Query("provider_id") int provider_id);

    @POST(Urls.removeNotification)
    Call<BaseResponse> remove(@Header("token") String token,
                              @Query("notification_id") int notification_id);
    @POST(Urls.ReportReview)
    Call<BaseResponse> repotreview(@Header("lang") String lang,
                                   @Header("token") String token,
                                   @Query("comment_id") int comment_id);

    @POST(Urls.ReportProvider)
    Call<BaseResponse> reportProvider(@Header("lang") String lang,
                                      @Header("token") String token,
                                      @Query("provider_id") String provider_id);


}
